package main
// Package declaration obligatory

// This is how imports are done
import (
	"fmt"
)

// main is a special name that means that this package is
// a program.
func main () {
	fmt.Println("Hello World")
}

// 'go run .' from the directory containing this
// file will compile and run this file. It doesn't
// keep the executable.

// 'go build .' will build an executable and leave
// it in the currend directory