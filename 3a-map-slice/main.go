package main

import (
	"fmt"
)

func main() {
	// We can create a map using make() or by
	// using a map literal
	makeExample()
	literalExample()
	// nilMap() // Causes panic
	assignToLiteral()
}

func makeExample() {
	var SIUnits = make(map[string]string)
	SIUnits["length"] = "meter"
	SIUnits["mass"] = "kilogram"
	fmt.Println(SIUnits)
}

func literalExample() {
	var metricUnits = map[string]string{
		"length": "meter",
		"mass":   "kilogram",
	}
	fmt.Println(metricUnits)

	// Shorthand for the above sysntax
	b := map[string]string{"a": "b"}
	fmt.Println(b)
}

func nilMap() {
	var britishUnits map[string]string
	britishUnits["length"] = "inch"
}

func assignToLiteral() {
	var metricUnits = map[string]string{
		"length": "meter",
		"mass":   "kilogram",
	}
	metricUnits["current"] = "ampere"
	fmt.Println(metricUnits)
}
