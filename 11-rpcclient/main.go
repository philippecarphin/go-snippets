package main

import (
	"fmt"
	"log"
	"net/rpc"
	"os"

	"gitlab.science.gc.ca/phc001/rpcclient/api"
)

func main() {

	// Create a client object: use rpc.DialHTTP()
	serverAddress := "localhost"
	client, err := rpc.DialHTTP("tcp", serverAddress+":1234")
	if err != nil {
		log.Fatal("dialing:", err)
	}

	// Obtain and print hostname
	host, err := os.Hostname()
	fmt.Printf("rpcclient:main() on host %s\n", host)

	// Prepare call : Declare an api.Args and an int for the reply
	args := api.Args{RHS: 7, LHS: 4}
	var reply int

	// Dispatch call and wait using client. Pass pointers to args and reply
	err = client.Call("Arithmetic.Add", &args, &reply)
	if err != nil {
		log.Fatal("arith error:", err)
	}

	// See that we have made a remote funciton call
	fmt.Printf("rpcclient:main() on host %s:  reply=%v\n", host, reply)
}
