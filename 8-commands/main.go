package main

import (
	"os"
	"os/exec"
	"fmt"
	"time"
	"bufio"
	"io"
)

func main () {
	// The high level way to run a commmand and get its output is to
	// use the Output method of exec.Command.
	getCommandOutput()

	// This demostrates communication with a process
	readPipeBuffered()
	readPipeRaw()
	// For if we do goroutines
	// time.Sleep(time.Second * 5)
}


// getCommandOutput() {
func getCommandOutput() {
	// create command object
	cmd := exec.Command("bash", "-c", "echo I am $USER@$(hostname)")
	// its output method runs the command and returns the contents of stdout ...
	out, _ := cmd.Output()
	// after completion, the exit code can be found in an attribute of cmd.ProcessState
	exitcode := cmd.ProcessState.ExitCode()
	fmt.Printf("process produced output '%s' and finished with exit code %d\n", out, exitcode)
}

func demoCmd(){
	cmd := exec.Command("bash")
	// Sets what the command will write to for stderr and stdout
	// This IS NOT what you read from to look at the output of
	// the command.
	_ = cmd.Stdout
	_ = cmd.Stderr

	// This is how you read from the output of the command: by
	// creating pipes.  These only make sense with cmd.Start()
	// but not with cmd.Wait()
	_, _ = cmd.StdoutPipe()
	_, _ = cmd.StderrPipe()

	// Start is asynchronous and we have Wait() to wait for the command
	// if we want.  Run() runs the command and waits for it to exit
	cmd.Start()
	cmd.Run()
	cmd.Wait()

	// We have various info attributes
	_ = cmd.Process.Pid
	_ = cmd.Process
	_ = cmd.ProcessState
	_ = cmd.ProcessState.ExitCode
	_ = cmd.ProcessState.Exited
}

// readPipeBuffered
func readPipeBuffered(){
	// We can read from the stdout of a command
	fmt.Println("SHIT")
	home := os.Getenv("HOME")
	cmd := exec.Command("ls", home)

	// We call the StdoutPipe method
	outPipe, _ := cmd.StdoutPipe()
	// We create a bufio.NewReader(that pipe)
	reader := bufio.NewReader(outPipe)
	// start the command
	cmd.Start()
	// In a loop
	for {
		// Use readstring method and compare err to io.EOF
		line, err := reader.ReadString('\n')
		if err == io.EOF {
			break
		}
		// do something with the line
		fmt.Printf("read line '%s'\n", line[0:len(line)-1])
		// time.Sleep to let it be nice
		time.Sleep(time.Millisecond * 100)
	}
}

// readPipeRaw()
func readPipeRaw() {
	// Create a command (echo something long enough)
	cmd := exec.Command("bash", "-c", "echo I am $USER@$(hostname)")
	// Get its StdoutPipe
	outPipe, _ := cmd.StdoutPipe()

	// Start
	cmd.Start()
	// Loop
	for {
		// bytes:= make([]byte, 8)
		bytes := make([]byte, 8)
		// nbRead,_ = call Read(bytes)
		nbRead, _ := outPipe.Read(bytes)
		// print what we got
		fmt.Printf("Read %d bytes : '%s'\n",nbRead, bytes)
		// if nbRead == 0 break
		if nbRead == 0 {
			break
		}
		time.Sleep(time.Second * 1)
	}
}
