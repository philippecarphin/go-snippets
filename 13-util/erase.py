
import sys

filename = sys.argv[1]

with open(filename, 'r') as f:
    lines = f.read().splitlines()

new_lines = [l if l.strip().startswith("//") else "" for l in lines]

with open(filename, 'w') as f:
    f.writelines('\n'.join(new_lines))