package main

import (
	"fmt"
	"os"
	"time"
)

// Pipeline will go input -> first() -> second() -> third()
// Invent three types so that input types to help us keep
// things straight when connecting things together
type atype int
type btype int
type ctype int

// Some constants to help with the demo (stepDuration:300, nbInputs)
const stepDuration = time.Millisecond * 300
const dt = time.Millisecond * 0
const nbInputs = 10
const bufferSize = 0

// func atype -> btype with time.Sleep(stepDuration)
func first(a atype) btype {
	time.Sleep(stepDuration + 1*dt)
	return btype(2 * a)
}

// func btype -> ctype with time.Sleep(stepDuration)
func second(b btype) ctype {
	time.Sleep(stepDuration + 2*dt)
	return ctype(3 * b)
}

// func ctype -> int with time.Sleep(stepDuration)
func third(c ctype) int {
	time.Sleep(stepDuration + 3*dt)
	return int(c * 4)
}

// main
func main() {
	// Make four channels IN -> first -> second -> third -> OUT
	aCh := make(chan atype, bufferSize)
	bCh := make(chan btype, bufferSize)
	cCh := make(chan ctype, bufferSize)
	iCh := make(chan int, bufferSize)

	// Make goroutinte to setup the pipeline
	// read from channel a, apply computation, send result on channel b
	go func() {
		for a := range aCh {
			bCh <- first(a)
		}
		close(bCh)
	}()
	// read from channel b, apply computation, send result on channel c
	go func() {
		for b := range bCh {
			cCh <- second(b)
		}
		close(cCh)
	}()
	// read from channel c, apply computation, send result on channel i
	go func() {
		for c := range cCh {
			iCh <- third(c)
		}
		close(iCh)
	}()

	// If len args == 1
	if len(os.Args) == 1 {
		// Single for loop, write on one end, wait for a thing to come out on the other end
		for i := 0; i < nbInputs; i++ {
			aCh <- atype(i)
			res := <-iCh
			fmt.Println(res)
		}
		// else
	} else {
		// Write stuff on the front of the pipeline
		go func() {
			for i := 0; i < nbInputs; i++ {
				time.Sleep(stepDuration)
				aCh <- atype(i)
			}
			close(aCh)
		}()

		// Wait for resutls on the other end of the pipeline
		for i := range iCh {
			fmt.Println(i)
		}
	}

}

// for later
// go func() {
// 	for i := range iCh {
// 		jCh <- fourth(i)
// 	}
// 	close(jCh)
// }()
// go func() {
// 	for j := range jCh {
// 		kCh <- fourth(j)
// 	}
// 	close(kCh)
// }()
// func fourth(i int) int {
// 	time.Sleep(stepDuration + 4*dt)
// 	return int(i * 4)
// }

// jCh := make(chan int, bufferSize)
// kCh := make(chan int, bufferSize)
