package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"time"
)

// function takes a filename, a variable, and an io.Writer
func tail(filename string, done *bool, out io.Writer) {

	// Open the filename with os.Open
	f, _ := os.Open(filename)
	// defer f.close()
	defer f.Close()
	// Create a bufio.NewReader
	rd := bufio.NewReader(f)
	// While True:
	for {
		// line, err from rd.ReadString('\n')
		line, err := rd.ReadString('\n')
		// Compare err to io.EOF
		if err == io.EOF {
			time.Sleep(time.Second)
			if *done {
				break
			}
		}
		out.Write([]byte(line))
		// check done variable and break if done
		// time.Sleep(time.Second * 1)
		// else write line to that io.Writer thing
	}
}

func main() {
	// async tail log1.txt
	var done bool = false
	go tail("log1.txt", &done, os.Stdout)
	fmt.Println("After starting first groutine")
	go tail("log2.txt", &done, os.Stdout)
	fmt.Println("After starting second groutine")
	// async tail log2.txt

	// Define this alias from the right directory
	// alias log="(cd $PWD && echo 'message to log 1' >> log1.txt && echo 'message to log 2' >> log2.txt)"
	for {
		time.Sleep(time.Second * 1)
	}
}
