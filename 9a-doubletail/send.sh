#!/bin/bash

# send output to the two files that we are tailing in our go routine
echo "to first $*" >> log1.txt
echo "to second $*" >> log2.txt
