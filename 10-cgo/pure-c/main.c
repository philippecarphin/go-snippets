#include <stdio.h>
#include ".real_header.h"

int main (void){
    int a = 9;
    int b = 8;
    int sum = Add(a, b);
    printf("The sum of %d and %d is %d\n", a, b, sum);
    char s[] = "World!";
    GoString msg = {s, sizeof(s)};
    SayHello(msg);
}