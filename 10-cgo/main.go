package main

import "C"
import (
	"fmt"
	"math"
	"sort"
	"sync"
)

var count int
var mtx sync.Mutex

//export Add
func Add(a, b int) int {
	fmt.Printf("GO : Adding %d and %d ...\n", a, b)
	return a + b
}

//export Cosine
func Cosine(x float64) float64 { return math.Cos(x) }

//export Sort
func Sort(vals []int) { sort.Ints(vals) }

//export Log
func Log(msg string) int {
	mtx.Lock()
	defer mtx.Unlock()
	fmt.Println(msg)
	count++
	return count
}

//export SayHello
func SayHello(msg string) int {
	fmt.Printf("This code written in go says 'Hello %s'\n", msg)
	return 0
}

func main() {}
