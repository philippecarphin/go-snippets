package main

import (
	"fmt"
	"log"
)

func main() {
	fmtDemo()
	logDemo()
}

func fmtDemo() {
	fmt.Printf("This is like C printf %s\n", "but with a bit more")
	fmt.Println("fmt.Println is useful too")
	// This is what I find myself using most to construct strings
	s := fmt.Sprintf("Like C sprintf but %s\n", "it returns the string")
	fmt.Print(s)
	err := fmt.Errorf("This function returns an error instance")
	fmt.Println(err)
}

func logDemo() {
	log.Printf("Log has lots of functions like fmt (Print, Printf, Println) but much more")
	// log.Fatalf("This line will stop the program if it is uncommented")
	log.Println("Logging can be configured for format, color, logging to file")
	log.Print("It is a good and easy to use module\n")
}
