package main

import (
	"fmt"
	"strings"
)

type TreeNode struct {
	Name     string
	Children []*TreeNode
}

var tree TreeNode = TreeNode{
	Name: "root",
	Children: []*TreeNode{
		&TreeNode{Name: "A",
			Children: []*TreeNode{
				&TreeNode{Name: "AA"},
			},
		},
	},
}

func (t *TreeNode) MarshalJSON() ([]byte, error) {

	childJsonList := make([]string, 0)
	for _, c := range t.Children {
		cj, err := c.MarshalJSON()
		if err != nil {
			return nil, err
		}
		childJsonList = append(childJsonList, string(cj))
	}

	inner := strings.Join(childJsonList, ",")

	out := fmt.Sprintf("{\"path\": \"%s\", \"children\": %s}", t.Name+"/A", string(inner))
	return []byte(out), nil
}

func main() {
	treeJson, _ := tree.MarshalJSON()
	fmt.Println(string(treeJson))
}
