package main

import (
	"fmt"
)

func main() {
	// There are four declarations: cont, var, func, type
	longConstVar()
	shortConstVar()
	colonEquals()
}
func longConstVar() {
	// Basic declarations : var, const, type, func
	const pi float64 = 3.14157
	var temp float64
	temp = 100
	// with var, the value can be left unassigned
	// at creation
	fmt.Printf("temp = %f, pi = %f\n", pi, temp)
}

func shortConstVar() {
	// Basic declarations : var, const, type, func
	// Type can be deduced
	const pi = 3.14157
	var temp = 100.0
	fmt.Printf("temp = %f, pi = %f\n", pi, temp)
}

func colonEquals() {
	// Most often used, shorthand for var (you still
	// to use const to declare constants)
	pi := 3.14157
	temp := 100.0
	fmt.Printf("temp = %f, pi = %f\n", pi, temp)
}
