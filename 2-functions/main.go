package main

import (
	"fmt"
)

func main() {
	myGoFunction("world")
}

func myGoFunction(name string) {
	fmt.Printf("Hello %s", name)
}
