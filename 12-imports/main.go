package main

import (
	"fmt"
	"os"

	// Import gitlab.science.gc.ca/phc001/cmcprompt/cmcprofile
	// optionnaly go get it first
	"gitlab.science.gc.ca/phc001/cmcprompt/cmcprofile"
)

func main() {
	// use ordenv version "1.2"
	version := "1.2"
	// Use "ORDENV_SITE_PROFILE" to show environment has changed
	variable := "ORDENV_SITE_PROFILE"

	// Show initial values
	fmt.Printf("Before %s=%s\n", variable, os.Getenv(variable))

	// Use external library
	// ====================

	// Print available versions of Ordenv
	fmt.Print(cmcprofile.GetVersions())
	// "acquire the environment"
	fmt.Printf("Acquiring ordenv environment %s\n", version)
	cmcprofile.AcquireOrdenvEnvironment(version)
	// prove that the environment has changed
	fmt.Printf("After %s=%s\n", variable, os.Getenv(variable))
}
