package main

import (
	// "io/ioutil"
	"strings"
	"bufio"
	"io"
	"os"
	"fmt"
)

func main(){
	demoOsReadFile()
	demoBufioNewReader()
	demoReadAndSplit()
}

// demoOsReadFile reads the content of a file into a string with
// os.ReadFile.
func demoOsReadFile(){
	content, err := os.ReadFile("main.go")
	if err != nil {
		panic(err)
	}
	fmt.Printf("File content : %s\n", content)
}

// demoBufioNewReader goes through a file line by line only storing
// one line at a time which could be desirable for large files.
// For small files see demoReadAndSplit
func demoBufioNewReader(){
	f, err := os.Open("main.go")
	if err != nil {
		panic(err)
	}

	rd := bufio.NewReader(f)

	var lineno int = 0
	for {
		line, err:= rd.ReadString('\n')
		if err == io.EOF {
			break;
		}

		lineno++
		fmt.Printf("Line %d: %s", lineno, line)
	}
}

func demoReadAndSplit(){
	content, err := os.ReadFile("main.go")
	if err != nil {
		panic(err)
	}

	linesSplit := strings.Split(string(content), "\n")
	for _, line := range linesSplit {
		fmt.Printf("line = %s\n", line)
		if line == "" {
			continue
		}
	}

	// https://stackoverflow.com/a/61938973/5795941
	// This filters out empty lines though we can easily skip them
	// with an if and a continue so I think I prefer the example
	// with strings.Split(., "\n").
	linesField := strings.FieldsFunc(string(content), func(c rune) bool { return c == '\n' || c == '\r' })
	for _, line := range linesField {
		fmt.Printf("line = %s\n", line)
	}
}

