package main

import (
	"fmt"
	"net/url"

	"github.com/RocketChat/Rocket.Chat.Go.SDK/models"
	"github.com/RocketChat/Rocket.Chat.Go.SDK/realtime"
	"github.com/howeyc/gopass"
)

func getpassword() string {
	s, _ := gopass.GetPasswd()
	return string(s)
}

func getToken() string {
	rocketChatURL, _ := url.Parse("https://message.gccollab.ca")
	client, err := realtime.NewClient(rocketChatURL, true)
	if err != nil {
		fmt.Println("Could not create client")
		return ""
	}
	creds := models.UserCredentials{
		Email:    "philippe.carphin2@canada.ca",
		Password: "password",
		Name:     "Philippe Carphin",
	}
	client.Login(&creds)
	chans, err := client.GetChannelsIn()
	if err != nil {
		fmt.Println("Could not get channels")
		return ""
	}
	for c := range chans {
		fmt.Printf("My channel %v\n", c)
	}

	// ms, err := client.LoadHistory("YMaRrsakACHuqLeE6ghYTuav7sPxGSfWf2")
	// if err != nil {
	// 	fmt.Println("Could not get History")
	// 	return ""
	// }
	// for m := range ms {
	// 	fmt.Println(m)
	// }
	// fmt.Printf("resp = %v\n", resp)
	// fmt.Printf("requestData = %v\n", requestData)
	return "143124314"
}

func main() {
	getToken()
}
